# The `docker-build.yml` File

This file contains the information about the docker dependencies and
build targets for a specific Git repository, and must reside in its root
directory. It may contain the following sections.

## `target`
Describes the build target (docker registry, username, repository name
and tags), together with the build arguments, so Dobby can build
multiple & different tags.

## `dependencies`
A list of dependencies, each with the following fields:

- `source` states the [check type](../README.md)
- check-specific options (see the check's documentation)
- `match` contains a regular expression to apply to the check result
  (e.g. if the version you want is is `vX.Y.Z-apache2`, you can use
  `match: "^v\\v-apache2$"`)
- `version` specifies the versions to consider (e.g. `>2.0.0`) - the
  latest considered version will then be the one used to determine if a
  build is required or not.

The `match` and `version` fields are present in most checks (but not in
all of them), more documentation on the contents of those two fields can
be found in [Version Matching](./version-matching.md).
