# Docker on Codeberg

1. Create a [`Dockerfile`](https://docs.docker.com/engine/reference/builder/) in your repository's root
2. Create a [`docker-build.yml`](./the-docker-build-file.md) file in your repository's root:
   ```yaml
   target:
     builds:
       - tags: ["latest"]
   ```
2. Push both to your default branch on Codeberg
3. Your image will now be automatically built and hosted, and you should see a "Docker" tab in your Repository. Failed builds will also be reported via email.

## Limitations
- Only for public repositories
- Only a single simultaneous build per user or organization
- 30 build minutes each day per user or organization (excluding pulling image dependencies)
- 25 GB of storage per user or organization (older versions will be removed 3 days after a warning via email)
- 1 GB maximum image size per build
- Limited during build to 4 GB of RAM, 2 CPU cores and 2 GB of temporary storage (including images dependencies)
- Only outgoing ports 80/443 (HTTP(S)), 22 (SSH), 21 (FTP) and 53 (DNS) are reachable during the build

Well-known projects or projects with a purpose that's directly relevant to our association's goals can also request more build minutes or resource limits via [contact@codeberg.org](mailto:contact@codeberg.org).

If you need support for private repositories or otherwise need to exceed those limitations for your own projects, you can also [host your own Dobby-based registry](./README.md).

## Rules
- The Dockerfiles' purpose must be only to build, test or package the application as expected when looking at the repository.
- It must not make excessive requests to external servers, or generally use excessive amount of resources for any purposes, except if that's directly neccessary for the build process.
- Breaking those rules might result in the Docker feature getting disabled for the repository, user or organization, or in the account being suspended in cases of obvious malicious usage of the service (like e.g. attacking external servers or mining cryptocurrency).
