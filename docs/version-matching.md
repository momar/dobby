# Version Matching
Regular expressions for version string matching can contain `\v` as an
optional token for semantic & shortened versions, effectively expanding
to `(?<version>(?:\d+\.){0,2}\d+)`.

The default matcher is ^v?\v.*$, matching most version strings like
"1.2.3", "v1-example", "v1.2b5" - you probably want to adjust that
especially when beta versions are released.

To compare the semantic versions gathered using the capturing group
named `version` (i.e. `\v`), the following operators can be used: (it's
also possible to use more than one, e.g. `>=1.2 <2`):

- `~` compatible (patch changed)
- `~~` compatible (minor version changed)
- `==` exact match
- `>=` at least
- `<=` at most
- `>` greater
- `<` less
- `*` always the latest version

## Library Usage
```javascript
import { Version } from "dobby";

const v = Version.parse("v1.2.3", "^v\\v$");
if (v !== null) {
    v.match("~1.2.2") // true
    v.match(">1 <2") // true
    v.match("==1.2.2") // false
}
```
