# Dobby: the house elf for your Docker images

### ⚠️ WARNING: README DRIVEN DEVELOPMENT

**This repository is using [Readme Driven Development](https://tom.preston-werner.com/2010/08/23/readme-driven-development.html), which means that everything you can read here are still ideas and plans for the future! The first public release will be some time in 2021.**

<!-- ### ➜ [Try it on Codeberg](./docs/codeberg.md) -->

---

It automatically builds your Docker images every time you push to your
git repo, or if a dependency of the image gets updated. You can just run
the daemon with the following `docker-compose.yml` file:

```yaml
version: "3"
services:
  dobby:
    image: momar/dobby
    restart: always
    command: # A list of the git repositories to watch
    - "git@codeberg.org:momar/dobby" # remote Git repo, local path in the container will be /var/lib/dobby/git-codeberg-org-momar-dobby/
    - "/var/lib/projects/example" # local Git repo
    volumes:
    # dobby will generate SSH keys here which can be used as deploy keys to access private repositories
    - ./git-deploy-keys:/etc/dobby/git-deploy-keys
    environment:
      check_interval: 15m
      registry_docker.io_username: "example"
      registry_docker.io_password: "access-token-or-password"
```

In your repository, you then need a file named
`docker-build.yml`, for example like this:

```yaml
target:
  repo: docker.io/momar/example
  builds:
    - tags: ["latest"]
    # You can also use the version of the package as the tag.
    # In this case, {version} is the full version, and {major}, {minor} and {patch} the fields from the version.
    - tags: ["v{version}", "v{major}.{minor}", "v{major}"]
      context: ./nodejs
      dockerfile: example.Dockerfile
      stage: dist
      args:
        # Specify build arguments here
        VERSION: "{version}"

dependencies:

# Check the git repository for updates.
# This is implied and can be left away, except if you want to build a specific branch.
- source: git
  repo: .
  branch: "master"

# Parse the Dockerfile and check every image referred to in a "FROM" statement.
# This is implied and can be left away, except if you want to check for Dockerfiles you're not building anything from.
- source: docker
  from: Dockerfile

# Match a JSON response using JSONPath.
- source: json
  url: "https://api.github.com/repos/abc/xyz/releases/latest"
  path: "$.tag_name"
  match: "^v\\v$"
  version: "~1.2.3"
```

Now, you can just use `docker-compose up -d` and it will automatically
build all your images using
[Docker-in-Docker](https://hub.docker.com/_/docker/).

## Documentation & Available Checks
- [The `docker-build.yml` File](./docs/the-docker-build-file.md)
- [Version Matching](./docs/version-matching.md)
- Generic Checks
  - [git](./docs/check-git.md)
  - [docker](./docs/check-docker.md)
  - [timer](./docs/check-timer.md)
  - [html](./docs/check-html.md)
  - [json](./docs/check-json.md)
  - [http](./docs/check-http.md)
- Specific Checks
  - [linux](./docs/check-linux.md)
  - [nodejs](./docs/check-nodejs.md)
  - [golang](./docs/check-golang.md)
  - [ruby](./docs/check-ruby.md)
  - [maven](./docs/check-maven.md)
  - [cargo](./docs/check-cargo.md)
